"""Example program of utilizing the orginal, full JLA data for cosmological
analysis.  This program performs MCMC analysis for the wCDM model.
"""
import sys
import numpy
import pymc
import sncosmology
# The modules "baselite" and "evaluatorlite" are watered-down versions of the
# "base" and "evaluator" modules in the libsncompress package.
import baselite
import evaluatorlite


inits = sys.argv[1]
init_floats = [float(x) for x in inits.split()]


n = baselite.BinnedSN("data/covmat", "data/jla_lcparams.txt")
ev = evaluatorlite.CovEvaluator(n)
ZS = n.redshifts
pzeroes = numpy.zeros_like(ZS)


om = pymc.Uniform("Omega_M", 0.0, 1.0, value=init_floats[0])
w = pymc.Uniform("w", -2.5, 1.0, value=init_floats[1])
M = pymc.Uniform("M", -5.0, 5.0, value=init_floats[2])
h = pymc.Normal("h", 0.688, 1.0 / (0.033 ** 2), value=init_floats[3])
alpha = pymc.Uniform("alpha", -1.0, 1.0, value=init_floats[4])
beta = pymc.Uniform("beta", -10.0, 10.0, value=init_floats[5])
delta = pymc.Uniform("delta", -0.5, 0.5, value=init_floats[6])


def _vec_cal(param):
    dist = sncosmology.abs_distmod_wcdm(ZS, param[:-1])
    vec = dist + param[-1]
    return vec


# The difference in theoretical predictions and data
@pymc.deterministic(plot=False, trace=False)
def vec(om=om, w=w, h=h, M=M, alpha=alpha, beta=beta, delta=delta):
    p = numpy.array([om, w, h, M])
    q = numpy.array([alpha, beta, delta])
    return _vec_cal(p) - ev.datavec(q)


# Parameter-dependent covariance
@pymc.deterministic(plot=False, trace=False)
def cov(alpha=alpha, beta=beta):
    q = numpy.array([alpha, beta])
    return ev.cov(q)


distmod = pymc.MvNormalCov("DistMod", mu=vec, C=cov, value=pzeroes,
                           observed=True)
model = [om, w, M, h, alpha, beta, delta, cov, vec, distmod]


if __name__ == "__main__":
    mc = pymc.MCMC(model,
                   db="hdf5",
                   dbname="output/postchain-full.hdf5",
                   dbcomplib="blosc",
                   dbcomplevel=3)
    mc.use_step_method(pymc.AdaptiveMetropolis, [M, h],
                       cov=numpy.array(((0.4, 0.05),
                                        (0.05, 0.1))),
                       shrink_if_necessary=True,
                       interval=500,
                       delay=10000)
    mc.use_step_method(pymc.AdaptiveMetropolis, [alpha, beta, delta],
                       cov=numpy.diag(numpy.array((3e-4, 9e-2, 15e-4))),
                       interval=500, shrink_if_necessary=True,
                       delay=5000)
    mc.sample(iter=520000, burn=20000)
    mc.summary()
    mc.db.close()
