"""Example program of utilizing the compressed JLA data for cosmological
analysis.  This program performs MCMC analysis for the wCDM model.

The output is an HDF5 file storing the chains.
"""
import sys
import numpy
import pymc
import sncosmology


# This is a very crude way of getting the initial values from the command line.
# Actually, the initial values are not that important, as it appears.
inits = sys.argv[1]
init_floats = [float(x) for x in inits.split()]


# Data loading.
# Full covariance of compressed data.
COV = numpy.loadtxt("data/table_A2.txt")
_mutab = numpy.loadtxt("data/table_A1.txt")
# Redshift table.
ZS = numpy.ascontiguousarray(_mutab[:, 0])
# Data table, with first 3 elements alpha, beta, and Delta_M.
PDATA = numpy.concatenate((numpy.loadtxt("data/table_post_compress_std.txt"),
                           _mutab[:, 1]))
del _mutab
# The above lines are tailored for the tabulated data in the journal paper
# (Tables A1 and A2).  The three post-compression parameters are read from 
# a separate table.  If you use the output of the "jlacompress" script, you
# don't need to do this, because the full post-compression data (with
# standardization parameters and the distance moduli) are already included in
# the same file.


# Model building.
# Specification of prior distributions for the parameters.
om = pymc.Uniform("Omega_M", 0.0, 1.0, value=init_floats[0])
w = pymc.Uniform("w", -2.5, 1.0, value=init_floats[1])
M = pymc.Uniform("M", -5.0, 5.0, value=init_floats[2])
h = pymc.Normal("h", 0.688, 1.0 / (0.033 ** 2), value=init_floats[3])
alpha = pymc.Uniform("alpha", -1.0, 1.0, value=init_floats[4])
beta = pymc.Uniform("beta", -10.0, 10.0, value=init_floats[5])
delta = pymc.Uniform("delta", -0.5, 0.5, value=init_floats[6])


def _vec_cal(p):
    """Implementing the logic of cosmological distance calculations.
    p: a 1-d array with elements in the order of Omega_M, w, h, M.
    Returns the model-prediction vector for the distance moduli that can be
    directly compared against the data values (by an M-correction).
    """
    dist = sncosmology.abs_distmod_wcdm(ZS, p[:-1])
    vec = dist + p[-1]
    return vec


# Prediction vector as a deterministic function of model parameters.
# Simply a wrapper to put all the variables together.
@pymc.deterministic(plot=False, trace=False)
def predvec(om=om, w=w, h=h, m=M, alpha=alpha, beta=beta, delta=delta):
    p = numpy.array([om, w, h, m])
    q = numpy.array([alpha, beta, delta])
    return numpy.concatenate([q, _vec_cal(p)])


# The conditional probability implementing the "inference" logic.
# This can be evaluated fairly fast because the covariance is a constant.
# It is also possible ot use the class pymc.MvNormalChol that takes the
# Cholesky factor of covariance instead of the covariance itself.  Here for
# the purpose of a direct illustration, we do not make this choice.
distmod = pymc.MvNormalCov("DistMod", mu=predvec, C=COV, value=PDATA,
                           observed=True)
# The model is built from all the random variables built earlier.
model = [om, w, M, h, alpha, beta, delta, predvec, distmod]


if __name__ == "__main__":
    # Sampling settings.
    # This script can be run repeatedly with the same output database.
    # The new chains will be added to the database file.
    mc = pymc.MCMC(model,
                   db="hdf5",
                   dbname=("output/postchain-compress.hdf5"),
                   dbcomplib="blosc",
                   dbcomplevel=3)
    # Use adaptive Metropolis sampling for (M, h).  This is optional.
    mc.use_step_method(pymc.AdaptiveMetropolis, [M, h],
                       cov=numpy.array(((0.4, 0.05),
                                        (0.05, 0.1))),
                       shrink_if_necessary=True,
                       interval=500,
                       delay=10000)
    # It's also optional to do this for the three standardization parameters.
    mc.use_step_method(pymc.AdaptiveMetropolis, [alpha, beta, delta],
                       cov=numpy.diag(numpy.array((3e-4, 9e-2, 15e-4))),
                       interval=500,
                       delay=5000,
                       shrink_if_necessary=True)
    # Start sampling, discarding the first 20,000 as burn-in.
    mc.sample(iter=520000, burn=20000)
    mc.summary()
    mc.db.close()
