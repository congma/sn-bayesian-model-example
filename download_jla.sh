#!/bin/sh
for _c in "$@"; do
    if [ "${_c}" = "-h" ]; then
	>&2 echo "usage: $0 [-c] [-h]"
	>&2 echo "  -c  clean up after download"
	>&2 echo "  -h  show this help and exit"
	exit 0
    elif [ "${_c}" = "-c" ]; then
	CLEAN=1
    fi
done

if [ -z "$DLCMD" ]; then
    if command -v wget > /dev/null 2>&1; then
	DLCMD="wget"
    elif command -v curl > /dev/null 2>&1; then
	DLCMD="curl"
    else
	>&2 echo "No wget/curl present"
	exit 1
    fi
fi

if [ "$DLCMD" = "wget" ]; then
    O="-O"
elif [ "$DLCMD" = "curl" ]; then
    O="-o"
fi

"$DLCMD" "$O" data/covmat_v6.tgz \
    http://supernovae.in2p3.fr/sdss_snls_jla/covmat_v6.tgz
sleep 2
"$DLCMD" "$O" data/jla_likelihood_v6.tgz \
    http://supernovae.in2p3.fr/sdss_snls_jla/jla_likelihood_v6.tgz

if [ "$?" = "0" ]; then
    cd data || exit 1
    tar zxf covmat_v6.tgz
    tar zxf jla_likelihood_v6.tgz jla_likelihood_v6/data/jla_lcparams.txt
    ln -s jla_likelihood_v6/data/jla_lcparams.txt
    if [ "$CLEAN" = "1" ]; then
	rm covmat_v6.tgz jla_likelihood_v6.tgz
    fi
    exit "$?"
else
    exit 1
fi
