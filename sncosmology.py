"""Distance evaluator for supernova cosmology."""
import numpy
import scipy.integrate
from wcdm_integrand import lumindist_wcdm_integrand_at


LGCM1 = numpy.log10(299792458.0)


def abs_distmod_wcdm(zseq, param):
    """Luminosity distance modulus (in physical values).
    zseq: a 1-d array-like container of redshift at which the distance moduli
          will be evaluated.
    param: a 1-d array-like container with at least 3 members, in the order
           of Omega_M (matter density), w (dark energy equation of state
           parmeter), and h (dimensionless Hubble constant).
    Returns an array in the same shape as zseq that contains the evaluated
    distance moduli.
    """
    # param: om, w, h
    h = param[2]
    p = param[:2]
    zs = numpy.atleast_1d(zseq)
    j = numpy.array([scipy.integrate.quad(lumindist_wcdm_integrand_at,
                                          0, z, args=p)[0] for z in zs])
    return 5.0 * (numpy.log10(j * (1.0 + zs) / h) + LGCM1)
