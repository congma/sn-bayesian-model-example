from distutils.core import setup, Extension
from numpy import get_include


setup(ext_modules=[Extension("wcdm_integrand",
                             ["wcdm_integrand.c"],
                             libraries=["m"],
                             include_dirs=[get_include()],
                             extra_compile_args=["-Wall",
                                                 "-Wextra",
                                                 "-pedantic",
                                                 "-std=c99",
                                                 "-O3",
                                                 "-march=native",
                                                 "-mtune=native"])])
