# vim: spell spelllang=en
import numpy


class CovEvaluator(object):
    """Likelihood evaluation for the binned supernovae.
    We follow the convention that the residual vector is data minus
    "model", not the other way around.

    Attributes:
        base:  BinnedSN instance
    """
    def __init__(self, basesn):
        """Set-up internal storage of covariance evaluator.
        Arguments:
            basesn:  BinnedSN instance
        """
        self.base = basesn
        return None

    def cov(self, param):
        """Evaluate the covariance of data at given parameter value."""
        alpha = param[0]
        beta = param[1]
        parray = numpy.array((1.0, alpha, beta))
        # The two alternative implementations of datacov can be considered
        # pairs of scale-load tradeoff.
        # Assuming some form of optimized BLAS/LAPACK.
        # The tensordot call will use the underlying multi-threaded
        # implementation.
        # This one seems to work better on low mem-bandwidth, low-scalability
        # machines by exploiting lightweight iteration of einsum and avoid
        # multi-threaded tensordot.  It should provide good enough performance
        # generically.
        datacov = numpy.einsum("ijkl, i, j", self.base.bblock, parray, parray)
        # This one seems to be better on high-bandwidth machines by exploiting
        # multi-threaded tensordot
        #datacov = numpy.einsum("ikl, i",
        #                       numpy.tensordot(self.base.bblock, parray,
        #                                       axes=(0, 0)),
        #                       parray)
        # This even more aggressive version seems to be OK for beefy
        # machines... but it doesn't necessarily result in better performance
        # of the whole program, despite being a bit faster in standalone bench.
        #datacov = numpy.tensordot(parray,
        #                          numpy.tensordot(self.base.bblock, parray,
        #                                          axes=(0, 0)),
        #                          axes=(0, 0))
        # symmetrize for stability?
        datacov = (datacov + datacov.T) / 2.0
        return datacov

    def datavec(self, param):
        """Evaluate the data vector (i.e. the distance moduli) at the parameter
        value.
        """
        alpha = param[0]
        beta = param[1]
        dmb = param[2]
        # NOTE: M_b^1 fixed to -19.05
        vec = (self.base.peakbmags +
               alpha * self.base.shapes +
               beta * self.base.colors + 19.05)
        # Host correction, mass-dependent term
        vec[self.base.needhostcorr] -= dmb
        return vec
