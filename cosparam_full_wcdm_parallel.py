"""Example program of utilizing the orginal, full JLA data for cosmological
analysis, with MPI parallel evaluator of the model distance.

This file is licensed under th GNU General Public License v3.0

See the file COPYING.gpl3 for details.
"""
import sys
import sncosmology
import parallelvicd
import baselite
import evaluatorlite


inits = sys.argv[1]
init_floats = [float(x) for x in inits.split()]


n = baselite.BinnedSN("data/covmat", "data/jla_lcparams.txt")
ev = evaluatorlite.CovEvaluator(n)
ZS = n.redshifts


# The parallel-worker function, conforming to the call signature
# function(instruction, data), although strictly speaking there's no such
# thing as a call signature in Python.  Here the cosmological parameters
# take the role of instruction broadcast to all the worker processes.
def worker(param, zseq):
    dist = sncosmology.abs_distmod_wcdm(zseq, param[:-1])
    vec = dist + param[-1]
    return vec


# The four-item instruction will be Omega_M, w, h, and M, on which the
# prediction vector depends
pair = parallelvicd.ManagerWorkerPair(worker, 4, ZS)


# The main process is confined to be executed in the manager process only.
# It handles everything that is not "evaluating the parallel-worker function",
# in particular all the IO.
@pair.confineto
def manager_main():
    import numpy
    import pymc
    pymc.set_threadpool_size(3)
    pzeroes = numpy.zeros_like(ZS)
    om = pymc.Uniform("Omega_M", 0.0, 1.0, value=init_floats[0])
    w = pymc.Uniform("w", -2.5, 1.0, value=init_floats[1])
    M = pymc.Uniform("M", -5.0, 5.0, value=init_floats[2])
    h = pymc.Normal("h", 0.688, 1.0 / (0.033 ** 2), value=init_floats[3])
    alpha = pymc.Uniform("alpha", -1.0, 1.0, value=init_floats[4])
    beta = pymc.Uniform("beta", -10.0, 10.0, value=init_floats[5])
    delta = pymc.Uniform("delta", -0.5, 0.5, value=init_floats[6])

    @pymc.deterministic(plot=False, trace=False)
    def vec(om=om, w=w, h=h, M=M, alpha=alpha, beta=beta, delta=delta):
        p = numpy.array([om, w, h, M])
        q = numpy.array([alpha, beta, delta])
        x1, x2 = pair.eval(p, ev.datavec, q)
        return x1 - x2

    @pymc.deterministic(plot=False, trace=False)
    def cov(alpha=alpha, beta=beta):
        q = numpy.array([alpha, beta])
        return ev.cov(q)

    distmod = pymc.MvNormalCov("DistMod", mu=vec, C=cov, value=pzeroes,
                               observed=True)
    model = [om, w, M, h, alpha, beta, delta, cov, vec, distmod]
    mc = pymc.MCMC(model,
                   db="hdf5",
                   dbname="output/postchain-full.hdf5",
                   dbcomplib="blosc",
                   dbcomplevel=3)
    mc.use_step_method(pymc.AdaptiveMetropolis, [M, h],
                       cov=numpy.array(((0.4, 0.05),
                                        (0.05, 0.1))),
                       shrink_if_necessary=True,
                       interval=500,
                       delay=10000)
    mc.use_step_method(pymc.AdaptiveMetropolis, [alpha, beta, delta],
                       cov=numpy.diag(numpy.array((3e-4, 9e-2, 15e-4))),
                       interval=500,
                       delay=5000,
                       shrink_if_necessary=True)
    mc.sample(iter=520000, burn=20000)
    mc.summary()
    mc.db.close()


if __name__ == "__main__":
    manager_main()

# The cleanup code will be executed by the manager after completion of the
# main function.  Despite its location here, the worker will never reach here
# during its normal execution, because it is trapped in a loop awaiting
# instructions.  When it does reach here due to the MANAGER calling
# terminate(), it will execute a terminate() as a noop and then the obligatory
# MPI_Finalize().
pair.terminate()
parallelvicd.finalize()
