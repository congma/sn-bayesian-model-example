Example Bayesian Modelling Code Accompanying the Paper "Application of 
Bayesian Graphs to SN Ia Data Analysis and Compression"

## Availability Note ##

This repository uses git submodules for accessing the Python module 
[`parallelvicd` hosted externally][pv].  To clone this repository will the 
external package, use the following command (available since git 1.6.5):

```bash
git clone --recursive https://gitlab.com/congma/sn-bayesian-model-example.git
```

If you do not specify the `--recursive` option, after a regular `git clone` 
you can `cd` into the repository directory and use the following command:

```bash
git submodule update --init
```

## Synopsis ##

This directory contains example computer code accompanying the paper 
"Application of Bayesian graphs to SN Ia data analysis and compression" (C.
Ma, P.-S. Corasaniti, & B. A. Bassett, 2016, [MNRAS, submitted][m16], "M16"; 
accepted version: 2016 MNRAS, 463, 1651, <abbr title="Digital Object Identifier">DOI</abbr>:
[`10.1093/mnras/stw2069`][m16a], BibCode: [`2016MNRAS.463.1651M`][m16ads]).

These programs are intended as examples rather than production code.  Please 
read the section "[Hacking](#hacking)" for more commentary.

The code is written in Python and uses the [`pymc`][pymc] library for Bayesian 
model-building and Monte Carlo simulation.

The following three Python scripts are example programs:

* [`cosparam_compress_wcdm.py`](cosparam_compress_wcdm.py): Analysis using 
  the compressed [JLA][jlarm] data obtained using the method in the M16 paper.
* [`cosparam_full_wcdm.py`](cosparam_full_wcdm.py): Same analysis, but using 
  the full <abbr title="Joint Light-curve Analysis">JLA</abbr> data.
* [`cosparam_full_wcdm_parallel.py`](cosparam_full_wcdm_parallel.py): 
  <abbr title="Message Passing Interface">MPI</abbr> parallel version of the 
  previous script.


## Prerequisites ##

### Data ###

The compressed JLA data described in M16 are already included in the directory 
"data" as three files:

* [`table_A1.txt`](table_A1.txt), [`table_A2.txt`](table_A2.txt): 
  Machine-readable versions of Tables A1 and A2 in M16, for the compressed 
  mean and covariance of JLA data.
* [`table_post_compress_std.txt`](table_post_compress_std.txt): 
  Post-compression standardization parameter means, corresponding to the first 
  column of Table 3 in M16.

The full JLA dataset is *not* included in this directory, and it must be 
downloaded separately.  Data files is hosted by the [JLA website][jlarm].

There is a simple convenient shell script, 
[`download_jla.sh`](download_jla.sh), that downloads the files, extract them 
from the archives, and put them into the paths expected by the example 
scripts.  Simply run the script from this directory as

```bash
./download_jla.sh
```

will suffice.  Optionally, it can be invoked as

```bash
./download_jla.sh -c
```

to clean up the unnecessary files after download.
 
### Code dependency ###

To run the example code, you need the following packages:

* [`pymc`][pymc], which further requires
* [`numpy`][numpy] and [`scipy`][scipy], among other dependencies.  The 
  `numpy`/`scipy` packages will benefit greatly from an optimized 
  implementation of <abbr title="Basic Linear Algebra Subroutines">BLAS</abbr> 
  and LAPACK linear algebra functions, such as [OpenBLAS][openblas], 
  [ATLAS][atlas], Intel [MKL][mkl], or Apple [Accelerate Framework][accf] (for 
  Mac OS X).
* [`pytables`][pytables] , as a dependency of `pymc`, for HDF5 storage of 
  chains.
* A C compiler, preferably [GCC][gcc] or [`clang`][clang], for compiling the C 
  extension code for evaluating the integrand in the expression of luminosity 
  distance for the wCDM model.  Without the C extension, numerical integration 
  is slow.
* Python and `numpy` header files, also for C compilation.  Usually included 
  with Python installation.

To perform the inference with full JLA dataset, you need the
[`astropy`][astropy] package, specifically the `astropy.io.fits` module, to
read the <abbr title="Flexible Image Transport System">FITS</abbr> tables.

(Formerly the [`pyfits`][pyfits] package was used.)

Additionally, the following software is required for running the MPI-enabled 
version of the full-JLA analysis script:

* An MPI implementation, such as [Open MPI][ompi].
* [`pypar`][pypar] for Python interfacing, and
* [`parallelvicd`][pv] as a workflow wrapper of `pypar`.  This is by the same 
  author and is included in the package [as a submodule](aux/parallelvicd), 
  and [a link to the package](parallelvicd) in the root directory is provided 
  for convenience.

### Compiling the C extension ###

To compile the C extension, run the script

```bash
./create_ext.sh
```

This will create the dynamically-loadable object `wcdm_integrand.so` that is 
compatible with the host system and the Python version.


## Usage ##

The Python scripts are intended as examples rather than fully-functional 
production code that can meet different needs directly.  They are more useful 
as readable code rather than executable.

However, to aide the execution of such scripts (which can be useful for 
reproducing the results in M16), I include a simple launcher script, 
[`launch.py`](launch.py).

You can see the options supported by the launcher with

```bash
./launch.py -h
```

To run the analysis script using compressed JLA data, typically you simply 
need

```bash
./launch.py cosparam_compress_wcdm.py
```

This will loop the script over multiple initial-value input in the file 
[`data/init-wcdm`](data/init-wcdm), producing a multi-chain HDF5 file as 
`output/postchain-compress.hdf5`.  Typically, this is fast enough to terminate 
in a few (~2) hours for the desired output of 2,000,000 MC samples.

In contrast, the full-JLA analysis will be substantially slower.  The 
single-process script can be launched in a similar manner by

```bash
./launch.py cosparam_full_wcdm.py
```

This will save the chain file `output/postchain-full.hdf5`.

The parallel full-JLA script can be started by

```bash
./launch.py -p cosparam_full_wcdm_parallel.py
```

By default, this will start 3 parallel processes.  This behaviour can be 
changed by passing additional arguments to the underlying MPI process starter
`mpiexec`, using the "`-x`" option to the launcher script.  For example, to 
increase the number of processes to 6, one can do the following

```bash
./launch.py -p -x '-n 6' cosparam_full_wcdm_parallel.py
```

Note the quoting -- `'-n 6'` is passed to the launcher script as a whole 
string via the "`-x`" option.  The launcher then will invoke `mpiexec` by

```
mpiexec -n 6 ... <the rest of the command-line arguments>
```


## Hacking ##

Each of the files

* `cosparam_compress_wcdm.py`
* `cosparam_full_wcdm.py`
* `cosparam_full_wcdm_parallel.py`

hopefully serves the purpose of providing some example on how to use `pymc` 
for model-building and inference.

These files are created to be self-contained and they can be executed as 
standalone Python scripts.  Each one takes one command-line argument, a 
*single string* containing 7 numbers separated by white-space characters, in 
the order of Omega_M, w, M, h, alpha, beta, Delta_M, as the initial values of 
Monte Carlo Markov Chains.  If you wish to pass such arguments to the scripts, 
you may need shell quoting or escaping to pass them as a single string.

The `launch.py` script manages this by pulling the parameters from a file, by 
default in [`data/init-wcdm`](data/init-wcdm), iteratively.

Some run-time information, such as the location of data files and MCMC 
parameters are hard-coded in the scripts.  Of course, in real production code 
better management of such information will be needed.

For better numerical precision, the distance in the wCDM model is evaluated by 
numerical integration using the fairly high default precision setting of the 
integrator `scipy.integrate.quad` (see the file 
[`sncosmology.py`](sncosmology.py)).  The package `scipy.integrate` mostly 
follows the [`quadpack`][qp] API.

To improve speed of quadrature, the integrand is provided as a C extension 
loadable by Python.

### Performance of covariance evaluation ###

In the file [`evaluatorlite.py`](evaluatorlite.py), specifically in the 
implementation of `CovEvaluator.cov` method, we include comments on the 
alternatives of data covariance implementation details.  They serve as some 
crude guidelines for improving evaluation speed for the full data covariance 
matrix on different machines.  However, it is our opinion that the optimal 
choice can only be determined by experiment.

### Parallel evaluation of distances ###

The parallel version of full-JLA analysis script can use more than one MPI 
"worker" processes to spread the load of evaluating the cosmological distance, 
which is quite time-consuming.

The idea is to split the job into "manager" and "worker" parts.  The worker 
processes evaluate a part of the redshift-to-distance jobs for the JLA 
dataset, and this is their only task.  The manager process distributes the 
cosmological parameters as it is generated by MCMC to the workers, gathers the 
distance evaluations, and performs the rest of the task including MCMC 
simulation and storage of chains.

Whether this results in better performance depends on the kind of single-unit 
processing power and scalability of your machine.  On computers with fast CPU 
cores but low scalability, the single-process version may perform better.
Otherwise, on machines with many but slower CPU cores, the parallel version 
may ease the evaluation by spreading the load.


## Permissions ##

Permission terms for these source code files

* `cosparam_compress_wcdm.py`
* `cosparam_full_wcdm.py`
* `baselite.py`
* `evaluatorlite.py`
* `sncosmology.py`
* `wcdm_integrand.c`
* `build_wcdm_cext.py`
* `create_ext.sh`
* `download_jla.sh`
* `launch.py`

are found in the file [`COPYING.bsd3cl`](COPYING.bsd3cl).

---

Permission terms for these source code files

* `aux/parallelvicd` and all files within that directory
* `cosparam_full_wcdm_parallel.py`

are found in the file [`COPYING.gpl3`](COPYING.gpl3).

---

Permissions to use, study, modify and redistribute the data files in the 
directory data, namely

* `data/table_A1.txt`
* `data/table_A2.txt`
* `data/table_post_compress_std.txt`
* `data/init-wcdm`

are granted, provided that the name of the original authors are properly 
attributed.  Note that this does not cover the JLA data files downloaded by 
the user.

[m16]: http://arxiv.org/abs/1603.08519 "The M16 paper (preprint)"
[m16a]: https://dx.doi.org/10.1093/mnras/stw2069 "The M16 paper (accepted version, subscription required)"
[m16ads]: http://adsabs.harvard.edu/abs/2016MNRAS.463.1651M "The M16 paper's entry in SAO/NASA ADS"
[pymc]: https://github.com/pymc-devs/pymc/ "PyMC"
[jlarm]: http://supernovae.in2p3.fr/sdss_snls_jla/ReadMe.html "JLA project"
[numpy]: http://www.numpy.org/ "NumPy homepage"
[scipy]: https://www.scipy.org/ "SciPy homepage"
[openblas]: https://github.com/xianyi/OpenBLAS "OpenBLAS"
[atlas]: http://math-atlas.sourceforge.net/ "Automatically Tuned Linear Algebra Software"
[mkl]: https://software.intel.com/en-us/intel-mkl/ "Math Kernel Library"
[accf]: https://developer.apple.com/library/tvos/documentation/Accelerate/Reference/AccelerateFWRef/index.html "Accelerate Framework"
[pytables]: http://www.pytables.org/ "PyTables"
[gcc]: https://gcc.gnu.org/ "GNU Compiler Collection"
[clang]: http://clang.llvm.org/ "clang: a C language family frontend for LLVM"
[pyfits]: http://www.stsci.edu/institute/software_hardware/pyfits "PyFITS"
[astropy]: http://www.astropy.org/ "Astropy"
[ompi]: https://www.open-mpi.org/ "Open MPI"
[pypar]: https://github.com/daleroberts/pypar/ "PyPar"
[pv]: https://github.com/congma/parallelvicd/ "parallelvicd"
[qp]: http://www.netlib.org/quadpack/ "QUADPACK"

<!--
vim: ft=markdown tw=78 fo+=tqwn spell spelllang=en_gb et ts=4
-->
