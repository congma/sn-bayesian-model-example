#!/usr/bin/env python
"""Rudimentary launcher script for both single-process and MPI example
scripts.  No real job control whatsoever.

Usage: launch.py [-h] [-i FILE] [-p] [-x ARGS] SCRIPT

See the output of launch.py -h for more.
"""
import sys
import os.path
import argparse
import subprocess
import time
import shlex


BASEDIR = os.path.dirname(os.path.abspath(sys.argv[0]))


def existpath(s):
    if not s:
        raise ValueError("empty string")
    if not os.path.exists(s):
        raise IOError("path not found")
    return s


def load_init(path):
    """Read the init lines from path.
    path: file path to read
    Returns a list containing pruned lines.
    """
    res = []
    with open(path, "r") as f:
        for line in f:
            potential_content = line.split("#")[0].strip()
            if potential_content:
                res.append(potential_content)
    return res


PARSER = argparse.ArgumentParser(description="launch an example script")
PARSER.add_argument("script",
                    metavar="SCRIPT",
                    help="script to launch",
                    type=existpath)
PARSER.add_argument("-i", "--init",
                    metavar="FILE",
                    help=("init parameter file "
                          "[default: data/init-wcdm under base directory of "
                          "this launcher script]"),
                    default=os.path.join(BASEDIR, "data/init-wcdm"),
                    type=existpath)
PARSER.add_argument("-p", "--mpi",
                    help=("use mpiexec to launch MPI parallel script "
                          "(must not use with non-MPI script)"),
                    action="store_true")
PARSER.add_argument("-x", "--mpiexec-args",
                    metavar="ARGS",
                    help=("additional arguments to mpiexec ["
                          "must be passed as a single string by quoting or "
                          "escaping, default: -n 3]"),
                    default="-n 3")


if __name__ == "__main__":
    p = PARSER.parse_args(sys.argv[1:])
    initlist = load_init(p.init)
    if not initlist:    # if initlist is empty
        sys.exit(0)     # vacuous success
    n_init = len(initlist)
    if p.mpi:
        e_cmd = ["mpiexec"] + shlex.split(p.mpiexec_args)
    else:
        e_cmd = []
    for i, init in enumerate(initlist):
        script_cmd = ["python", p.script, init]
        cmd = e_cmd + script_cmd
        r = subprocess.call(cmd)
        now = time.strftime(r"%F %R:%S")
        print >> sys.stderr, ("[%s] [%s]: "
                              "job %d of %d finished (status %d)" %
                              (now, p.script, i + 1, n_init, r))
        if r != 0:
            sys.exit(r)
