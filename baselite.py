# vim: spell spelllang=en
import os.path
import numpy
import astropy.io.fits


# Table 9 of B14
_SIGMA_COH_TABLE = {1: 0.08,     # SNLS
                    2: 0.108,    # SDSS-II
                    3: 0.134,    # low-z, CfA
                    4: 0.1,      # HST
                    }
_COEFF_SIGMAZ = 1.0864878442920631136702801549045434e-3


def _preeval_diag_in_cov(redshifts, setnum):
    """Pre-compute the constant diagonal due to peculiar redshift, lensing
    and intrinsics from data table.
    Returns the constant table.
    """
    tsize = redshifts.shape[0]
    diag = numpy.square(_COEFF_SIGMAZ / redshifts)      # Peculiar redshift
    diag += numpy.square(redshifts * 0.055)             # Lensing
    for i in xrange(tsize):                             # Intrinsic
        diag[i] += _SIGMA_COH_TABLE[setnum[i]] ** 2
    return diag


def loadcovbase(dirpath):
    """Read the "base" covariance matrix from the FITS files stored under
    the directory dirpath.
    """
    tmp = []
    for basename in os.listdir(dirpath):
        if basename.endswith(".fits"):
            fitsfile = astropy.io.fits.open(os.path.join(dirpath, basename))
            tmp.append(numpy.asarray(fitsfile[0].data))
            fitsfile.close()
    if not tmp:     # Data absent
        raise ValueError("cannot find sum of data covariance matrices")
    res = sum(tmp)
    return res


def loadsntable(path):
    """Read the supernova table from given data file located at path.
    Returns a numpy ndarray object, in which each row is a record
    and each column contain values for an attribute.  Only the useful ones
    are returned.
    """
    columns = (1,   # z, redshift
               4,   # m_b, peak B-magnitude
               6,   # X_1, shape parameter
               8,   # C, color parameter
               10,  # M_stellar, stellar mass of host galaxy
               17,  # dataset identifier
               )
    fulltable = numpy.loadtxt(path, usecols=columns)
    return fulltable


def _create_base_block(basematrix):
    """Construct the "base block", a space-time trade-off for faster accessing
    the base matrix.

    Arguments:
        basematrix:  the "base" matrix, as given by the binary data file
        themselves

    Return value:
        A numpy.ndarray object, B, of shape (3, 3, N, N), where
        B[i][j] is the ij-th block matrix of covariances. i, j in {0, 1, 2},
        which corresponds to peak B-magnitude, stretch, and color parameters
        respectively.
    """
    N = basematrix.shape[0]
    assert N == basematrix.shape[1]
    assert not (N % 3)
    M = N // 3
    bblock = numpy.empty((3, 3, M, M))
    for i in xrange(3):
        for j in xrange(3):
            bblock[i][j] = basematrix[i::3, j::3]
    return bblock


class BinnedSN(object):
    """Binned supernova data.
    An instance of this class BinnedSN, after initialization, contains the
    following attributes that give access to the constants needed for
    computation:
        basematrix: Base covariance matrix for the observables,
                    corresponding to C_eta in B14.
        table: Array containing the data points.  Each row is a record
               for a point, and the columns are organized as follows:
                   0: z, redshift
                   1: m_b, peak B-magnitude
                   2: X_1, shape parameter
                   3: C, color parameter
                   4: M_stellar, stellar mass of host galaxy
                   5: dataset identifier
               Short-hand attributes are available for accessing copies
               of the table columns.  The columns has the following synonyms:
                   0: redshifts
                   1: peakbmags
                   2: shapes
                   3: colors
                   4: mstellar
                   5: dataset
        datadimension: Number of data points.
        needhostcorr: Boolean array, precomputed for the predicate
                      M_stellar >= 10
    """
    def __init__(self, basedirpath, tablepath):
        """Initialize a BinnedSN instance by loading the data files
        and performing binning.

        basedirpath: Path to directory containing the FITS files
                     for various components in the base covariance
                     matrix.
        tablepath: Path to the text file containing the data table.
        """
        basematrix = loadcovbase(basedirpath)
        # Symmetrize?
        basematrix = (basematrix + basematrix.T) / 2.0
        table = loadsntable(tablepath)
        self.table = numpy.ascontiguousarray(table)
        self.bblock = _create_base_block(basematrix)
        self.needhostcorr = self.table[:, 4] >= 10
        # Create "familiar" names, or views, for accessing table elements.
        self.redshifts = numpy.ascontiguousarray(self.table[:, 0])
        self.logredshifts = numpy.log10(self.redshifts)
        self.peakbmags = numpy.ascontiguousarray(self.table[:, 1])
        self.shapes = numpy.ascontiguousarray(self.table[:, 2])
        self.colors = numpy.ascontiguousarray(self.table[:, 3])
        self.mstellar = numpy.ascontiguousarray(self.table[:, 4])
        # This is not a view but a copy
        self.dataset = numpy.ascontiguousarray(self.table[:, 5], dtype=int)
        # Update the 00-block of bblock by the diagonal augment.
        # The meaning of this update is in Equation (13) of B14.
        self.bblock[0][0] += numpy.diag(_preeval_diag_in_cov(self.redshifts,
                                                             self.dataset))
        return None
