#include "Python.h"
#include <math.h>
#include "numpy/arrayobject.h"


/*
 * Takes the first two elements as (C) double from a sequence-like object.
 * Intended to be in-lined by the compiler.  Unrolled by hand.
 */
static inline int
first_2_double_from_pyseq(PyObject * const maybe_seq,
	double * restrict const e0, double * restrict const e1)
{
    double v;
    PyObject * r = NULL;

    if ( !PySequence_Check(maybe_seq) )
	goto fail;

    if ( !(r = PySequence_ITEM(maybe_seq, 0)) )
	goto fail;
    if ( (v = PyFloat_AsDouble(r)) == -1.0 && PyErr_Occurred() )
	goto cleanup;
    *e0 = v;
    Py_DECREF(r);

    if ( !(r = PySequence_ITEM(maybe_seq, 1)) )
	goto fail;
    if ( (v = PyFloat_AsDouble(r)) == -1.0 && PyErr_Occurred() )
	goto cleanup;
    *e1 = v;
    Py_DECREF(r);

    return 0;

cleanup:
    Py_DECREF(r);

fail:
    return 1;
}


/*
 * The integrand of wcdm luminosity distance calculations (one over
 * dimensionless expansion rate).
 * Returns Python float as new reference.  Uses double for internal
 * floating-point calculations.
 */
static PyObject *
wcdmint_at(PyObject * self, PyObject * args)
{
    PyObject * z_obj, * param_obj;
    double z, om, w, ol, pz3, tmp;

    /* Two arguments: z and param */
    if ( !PyArg_ParseTuple(args,
		"OO:lumindist_wcdm_integrand_at",
		&z_obj, &param_obj) ) {
	PyErr_BadArgument();
	return NULL;
    }

    /* Get z as a C native number */
    z = PyFloat_AsDouble(z_obj);
    if ( (z == -1.0) && PyErr_Occurred() ) {
	PyErr_SetString(PyExc_ValueError,
		"z cannot be converted to double");
	return NULL;
    }

    /* Get om and w as first 2 sequence members */
    if ( first_2_double_from_pyseq(param_obj, &om, &w) ) {
	PyErr_SetString(PyExc_ValueError,
		"cannot get the first 2 elements as floats from param");
	return NULL;
    }

    /* The actual scoop */
    z += 1.0;
    ol = 1.0 - om;
    pz3 = z * z * z;
    tmp = fma(ol, pow(pz3, w), om) * pz3;
    tmp = 1.0 / sqrt(tmp);

    return PyFloat_FromDouble(tmp);
}


static PyMethodDef WMethods[] =
{
    {
	"lumindist_wcdm_integrand_at",
	wcdmint_at,
	METH_VARARGS,
	"lumindist_wcdm_integrand_at(z, param) -> "
	    "Integrand of WCDM distance function at given z"
    },
    {NULL, NULL, 0, NULL}
};


PyMODINIT_FUNC
initwcdm_integrand(void)
{
    PyObject * m;
    /* if initialization fails */
    if ( !(m = Py_InitModule("wcdm_integrand", WMethods)) )
	return;
    import_array();
}
